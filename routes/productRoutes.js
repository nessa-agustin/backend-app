const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth');


router.post('/',auth.verify,productController.addProduct);

router.get('/',productController.getAllActiveProducts);
router.get('/all',auth.verify,productController.getAllProducts);
router.get('/:id',productController.getProduct);

router.put('/:id/update',auth.verify,productController.updateProduct);
router.put('/:id/archive',auth.verify,productController.archiveProduct);

module.exports = router;